theming

- [ ] minimal
- [ ] modern
- [ ] retro
- [x] osx
- [ ] future

features

- [ ] animations
- [ ] some sort of data viz thing
- [ ] some sort of map
- [ ] functioning auth and comment system
- [ ] functioning and styled payments
- [ ] drawing board
- [ ] techincal skills
