import type {SvelteComponent} from 'svelte'

export interface Theme {
  font: string
  pal: {
    black: string
    red: string
    green: string
    yellow: string
    blue: string
    purple: string
    cyan: string
    white: string
    bg: string
  }
  card: SvelteComponent | null
  image: string
}

interface Themes {
  [themename: string]: Theme
}

const themes: Themes = {
  Minimal: {
    font: 'Lato',
    pal: {
      black: '#00000',
      red: '#CC3E28',
      green: '#02e53b',
      yellow: '#FFFF00',
      blue: '#0000FF',
      purple: '#800080',
      cyan: '#00FFFF',
      white: '#FFFFFF',
    },
    card: null,
  },
  Solarized: {
    font: 'Lora',
    pal: {
      black: '#073642',
      red: '#dc322f',
      green: '#859900',
      yellow: '#b58900',
      blue: '#268bd2',
      purple: '#d33682',
      cyan: '#2aa198',
      white: '#fdf6e3',
    },
    card: null,
  },
  Material: {
    font: 'Roboto',
    pal: {
      black: '#000000',
      red: '#ef5350',
      green: '#4caf50',
      yellow: '#ff9800',
      blue: '#42a5f5',
      purple: '#ba68c8',
      cyan: '#03a9f4',
      white: '#FFFFFF',
    },
    card: null,
  },
  Retro: {
    font: 'Fira Code',
    pal: {
      black: '#19282F',
      red: '#B33030',
      green: '#8BD881',
      yellow: '#FFE162',
      blue: '#1C5DD0',
      purple: '#6A5495',
      cyan: '#A3E4DB',
      white: '#F5F5F5',
    },
    card: null,
  },
  'OS X': {
    font: 'Helvetica',
    pal: {
      black: '#000000',
      red: '#ff453a',
      green: '#28cd41',
      yellow: '#ffcc00',
      blue: '#007aff',
      purple: '##af52de',
      cyan: '#55bef0',
      white: '#fafafa',
      bg: '#dedede',
    },
    card: null,
    image: '/osx-bg.jpg',
  },
}

export default themes
