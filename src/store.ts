import {writable} from 'svelte/store'
import themes from './themes'

export const theme = writable(themes[0])
